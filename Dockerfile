FROM openjdk:11.0.5-jre-slim
VOLUME /tmp

ADD application/build/libs/application-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8080

ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar
